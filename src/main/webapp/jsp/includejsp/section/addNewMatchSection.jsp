<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${applicationScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>

<div class="win match" style="display:none;">
    <div class="overlay"></div>
    <div class="visible">
        <div class="content-popup">
            <form id="add-match" method="post" action="/controller">
                <input type="hidden" name="command" value="add_sport">
                <table>
                    <thead>
                    <tr>
                        <th><fmt:message key="sport.nameSport"/></th>
                        <th><fmt:message key="sport.members"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <select name="name_sport" required>
                                <option disabled><fmt:message key="sport.chooseSport"/></option>
                                <option selected value="Football"><fmt:message key="sport.football"/></option>
                                <option value="Basketball"><fmt:message key="sport.basketball"/></option>
                                <option value="Volleyball"><fmt:message key="sport.volleyball"/></option>
                                <option value="Mini_football"><fmt:message key="sport.miniFootball"/></option>
                                <option value="Handball"><fmt:message key="sport.handball"/></option>
                                <option value="Hockey"><fmt:message key="sport.hockey"/></option>
                            </select>
                        <td>
                            <input type="text" value="" name="owner">
                            <input type="text" value="" name="guest">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <input type="submit" form="add-match" value="Добавить">
        <button type="button" onClick="$('.win.match').hide();"><fmt:message
                key="close"/>
        </button>
    </div>
</div>