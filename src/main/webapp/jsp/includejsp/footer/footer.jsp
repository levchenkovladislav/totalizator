<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${applicationScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<span>
    <fmt:message key="footer.copyright"/>
</span>