$(document).ready(function () {
    var webSocket = new WebSocket("ws://localhost:8080/chat");
    var messages = document.getElementById("messages");
    var message = document.getElementById("message");
    messages.value += "\n\n";

    $("#send-message").click(function () {
        webSocket.send(message.value);
        message.value = "";
    });

    $("#message").keydown(function (e) {
        if (e.keyCode === 13) {
            webSocket.send(message.value);
        }
    });

    $("#message").keyup(function (e) {
        if (e.keyCode === 13) {
            message.value = "";
        }
    });

    webSocket.onmessage = function (message) {
        messages.value += message.data + "\n";
        messages.scrollTop = messages.scrollHeight;
    };
});