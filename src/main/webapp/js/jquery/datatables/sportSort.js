$(function () {
    var locale = $("meta[name=locale]").attr("content");
    console.log(locale);
    $('#tab-three .table-data-tables').dataTable({
        "language": {
            "url": "/js/jquery/datatables/lang/dataTableLang_" + locale + ".lang"
        },
        "columnDefs": [
            { "type": "numeric", "targets": 1 }
        ],
        // "bLengthChange" : false,
    });

    $('#users .table-data-tables').dataTable({
        "language": {
            "url": "/js/jquery/datatables/lang/dataTableLang_" + locale + ".lang"
        },
        // "bLengthChange" : false,
    });

    $('#tab-four .table-data-tables').dataTable({
        "language": {
            "url": "/js/jquery/datatables/lang/dataTableLang_" + locale + ".lang"
        },
        "bSort": false,
        // "bLengthChange" : false,
    });
});
