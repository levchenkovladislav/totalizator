$(function(){
    $('#tabs .tab-container').hide();
    getactiveTab = $(".tabs-menu li.active a").attr('data-id');
    $("#"+getactiveTab).show();

    $('.tabs-menu a').on('click touchend',function(event){
        event.preventDefault()
        $(this).parents('.tabs-menu').find('li').removeClass('active');
        $(this).parent().addClass('active');
        var currentTab = $(this).attr('data-id');
        $(this).parents('#tabs').find('.tab-container').hide();
        $("#"+currentTab).show();
    });
});