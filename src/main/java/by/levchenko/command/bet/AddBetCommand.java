package by.levchenko.command.bet;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.dao.entity.User;
import by.levchenko.logic.action.BetAction;
import by.levchenko.logic.exception.BetException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * A command that is executed when the client makes a bet.
 * @author Vladislav Levchenko
 */

public class AddBetCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(AddBetCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.sport");

        String valueParam = request.getParameter("value");
        String coefficientIdParam = request.getParameter("coeffId");
        User user = (User) request.getSession().getAttribute("user");


        if (valueParam == null || coefficientIdParam == null) {
            page = ConfigurationManager.getProperty("path.page.index");
            return page;
        }
        String status = "";
        BigDecimal value = new BigDecimal(valueParam);
        if (value.compareTo(BigDecimal.valueOf(0)) <= 0) {
            status = MessageManager.getProperty("message.badBet");
            request.setAttribute("status", status);
            return page;
        }
        int coefficientId = Integer.parseInt(coefficientIdParam);

        try(BetAction betAction = new BetAction()) {
            if(betAction.doBet(value, coefficientId, user)){
                status = MessageManager.getProperty("message.success");
            }
        } catch (BetException e) {
            status = MessageManager.getProperty("message.cantDoBet");
            Logger.log(Level.WARN, e);
        } catch (Exception e) {
            status = MessageManager.getProperty("message.fail");
            Logger.log(Level.ERROR, e);
        }

        request.setAttribute("status", status);

        return page;
    }
}
