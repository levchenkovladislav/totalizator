package by.levchenko.command.sport;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.dao.entity.User;
import by.levchenko.logic.action.SportAction;
import by.levchenko.logic.exception.SportException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the administrator creates the match.
 *
 * @author Vladislav Levchenko
 */

public class AddSportCommand implements ActionCommand {


    private static Logger Logger = LogManager.getLogger(AddSportCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.sport");

        User user = (User) request.getSession().getAttribute("user");

        String nameSport = request.getParameter("name_sport");
        String owner = request.getParameter("owner");
        String guest = request.getParameter("guest");

        request.setAttribute("status", "fail");

        if (nameSport == null || owner == null || guest == null) {
            page = ConfigurationManager.getProperty("path.page.index");
            return page;
        }

        try (SportAction sportAction = new SportAction()) {
            if (sportAction.addSport(user.getUsername(), nameSport, owner, guest)) {
                request.setAttribute("status", MessageManager.getProperty("message.success"));
                new PrintSportsCommand().execute(request);
            }
        } catch (SportException e) {
            request.setAttribute("status", MessageManager.getProperty("message.cantAddSport"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }


        return page;
    }
}
