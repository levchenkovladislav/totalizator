package by.levchenko.command.sport;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.logic.action.CoeffAction;
import by.levchenko.logic.action.SportAction;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.FutureTask;

/**
 * A command that is executed when the user wants to see the page with list of the match.
 *
 * @author Vladislav Levchenko
 */

public class    PrintSportsCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(PrintSportsCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.sport");

        try (CoeffAction coeffAction = new CoeffAction();
             SportAction sportAction = new SportAction()) {
            FutureTask sportForJSPTask = new FutureTask(() -> sportAction.findAll(false));////
            new Thread(sportForJSPTask).start();
            FutureTask categoryCoeffTask = new FutureTask(() -> coeffAction.findAllCategoryCoeff());
            new Thread(categoryCoeffTask).start();

            request.getSession().setAttribute("coefficients", sportForJSPTask.get());
            request.getSession().setAttribute("categoryCoeffs", categoryCoeffTask.get());
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
