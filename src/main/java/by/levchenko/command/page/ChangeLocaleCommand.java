package by.levchenko.command.page;


import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.UtilCommand;

import javax.servlet.http.HttpServletRequest;

/**
 * The command that is executed when the user edits language.
 *
 * @author Vladislav Levchenko
 */

public class ChangeLocaleCommand implements ActionCommand {
    private static final String PARAM_LANG_EN = "EN";
    private static final String PARAM_LANG_RU = "RU";
    private static final String PARAM_LANG_BY = "BY";
    private static final String PARAM_LANG_ru_RU = "ru_RU";
    private static final String PARAM_LANG_en_US = "en_US";
    private static final String PARAM_LANG_be_BY = "be_BY";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    public String execute(HttpServletRequest request) {
        String locale = "locale";

        String page = UtilCommand.definePage(request);


        String parameter = request.getParameter(locale);

        parameter = parameter != null ? parameter : "";

        switch (parameter) {
            case PARAM_LANG_EN:
                locale = PARAM_LANG_en_US;
                break;
            case PARAM_LANG_RU:
                locale = PARAM_LANG_ru_RU;
                break;
            case PARAM_LANG_BY:
                locale = PARAM_LANG_be_BY;
                break;
            default:
                locale = "";
        }

        MessageManager.changeLocale(locale);
        request.getServletContext().setAttribute("locale", locale);

        return page;
    }
}
