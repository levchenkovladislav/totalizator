package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.dao.entity.User;
import by.levchenko.dao.entity.UserRole;
import by.levchenko.logic.action.UserAction;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * A command that is executed when the administrator deletes user.
 *
 * @author Vladislav Levchenko
 */

public class DeleteUserCommand implements ActionCommand {


    private static Logger Logger = LogManager.getLogger(DeleteUserCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {

        String page = ConfigurationManager.getProperty("path.page.controller.print_users");

        User user = (User) request.getSession().getAttribute("user");
        UserRole userRole = UtilCommand.defineUserRole(request);

        String username = request.getParameter("username");

        if (username == null || request.getParameter("userId") == null) {
            page = ConfigurationManager.getProperty("path.page.index");
            return page;
        }

        int userId = Integer.parseInt(request.getParameter("userId"));
        if (userId == user.getId()) {
            request.setAttribute("status", MessageManager.getProperty("message.cantDeleteYourself"));
            return page;
        }

        UserRole role = UserRole.valueOf(request.getParameter("role"));

        try (UserAction userAction = new UserAction()) {
            if (userAction.deleteUser(userId, role)) {
                request.setAttribute("status", MessageManager.getProperty("message.success"));
            } else {
                request.setAttribute("status", MessageManager.getProperty("message.fail"));
            }
            page = ConfigurationManager.getProperty("path.page.users");
            ArrayList<User> users = userAction.findAll();
            request.getSession().setAttribute("users", users);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
