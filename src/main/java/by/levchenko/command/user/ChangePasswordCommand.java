package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.dao.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user changes the password.
 *
 * @author Vladislav Levchenko
 */

public class ChangePasswordCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(ChangePasswordCommand.class);

    private static final String PARAM_PARAM_CURRENT_PASSWORD = "current_password";
    private static final String PARAM_NEW_PASSWORD = "new_password";
    private static final String PARAM_AGAIN_PASSWORD = "again_password";
    private static final String PATTERN_PASSWORD = "^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S{8,}$";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.userAccount");

        String current_password = request.getParameter(PARAM_PARAM_CURRENT_PASSWORD);
        String new_password = request.getParameter(PARAM_NEW_PASSWORD);
        String again_password = request.getParameter(PARAM_AGAIN_PASSWORD);

        User user = (User) request.getSession().getAttribute("user");

        if (current_password == null || new_password == null || again_password == null) {
            page = ConfigurationManager.getProperty("path.page.index");
            return page;
        }

        if (!new_password.equals(again_password)) {
            request.setAttribute("status", MessageManager.getProperty("message.passwordsDontMatch"));
            return page;
        }


        try (UserAction userAction = new UserAction()) {
            if (!userAction.checkPassword(current_password, user.getPassword())) {
                request.setAttribute("status", MessageManager.getProperty("message.incorrectPassword"));
                return page;
            }
            if(!userAction.checkPassWithRegexp(new_password, PATTERN_PASSWORD)){
                request.setAttribute("status",
                        MessageManager.getProperty("message.incorrectPasswordRegex"));
                return page;
            }

            if (userAction.changePassword(user, new_password)) {
                user.setPassword(new_password);
                request.setAttribute("status", MessageManager.getProperty("message.success"));
            } else {
                request.setAttribute("status", MessageManager.getProperty("message.fail"));
            }
        } catch (UserException e) {
            request.setAttribute("status", MessageManager.getProperty("message.cantChangePassword"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
