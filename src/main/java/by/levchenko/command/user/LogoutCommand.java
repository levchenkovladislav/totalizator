package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user wats to logout.
 *
 * @author Vladislav Levchenko
 */

public class LogoutCommand implements ActionCommand {
    private static final String PARAM_LOCALE = "locale";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.index");

        String locale = (String) request.getSession().getAttribute(PARAM_LOCALE);
        request.getSession().invalidate();
        request.getSession().setAttribute(PARAM_LOCALE,locale);
        return page;
    }
}