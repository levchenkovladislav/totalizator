package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.logic.action.UserAction;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.FutureTask;

/**
 * A command that is executed when the administrator wants to see the page with list of users.
 *
 * @author Vladislav Levchenko
 */

public class PrintUsersCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(PrintUsersCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {

        String page = ConfigurationManager.getProperty("path.page.users");

        try (UserAction userAction = new UserAction()){
            FutureTask usersTask = new FutureTask(() -> userAction.findAll());
            new Thread(usersTask).start();

            request.getSession().setAttribute("users", usersTask.get());
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
