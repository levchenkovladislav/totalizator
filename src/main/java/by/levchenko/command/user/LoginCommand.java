package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.dao.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user wats to login.
 *
 * @author Vladislav Levchenko
 */

public class LoginCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(LoginCommand.class);

    private static final String PARAM_NAME_LOGIN = "username";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PATTERN_PASSWORD = "^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S{8,}$";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);

        if (login == null || pass == null) {
            page = ConfigurationManager.getProperty("path.page.index");
            return page;
        }

        page = ConfigurationManager.getProperty("path.page.login");

        try(UserAction userAction = new UserAction()) {
            User user = userAction.checkLoginAndPass(login, pass);
            request.getSession().invalidate();
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("userRole", user.getRole());
            page = ConfigurationManager.getProperty("path.page.index");
        } catch (UserException e) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.loginError"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }
        return page;
    }
}