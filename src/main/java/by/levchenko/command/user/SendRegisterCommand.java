package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.dao.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed to send the link to register.
 *
 * @author Vladislav Levchenko
 */

public class SendRegisterCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(SendRegisterCommand.class);

    private static final String PARAM_NAME_LOGIN = "username";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_EMAIL = "email";
    private static final String PATTERN_PASSWORD = "^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S{8,}$";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");

        String page = ConfigurationManager.getProperty(user != null ? "path.page.users" : "path.page.login");

        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        String email = request.getParameter(PARAM_NAME_EMAIL);
        String role = request.getParameter("role");

        if (login == null || pass == null || email == null) {
            page = ConfigurationManager.getProperty("path.page.index");
            return page;
        }
        try (UserAction userAction = new UserAction()) {
            if (!userAction.checkPassWithRegexp(pass, PATTERN_PASSWORD)) {
                request.setAttribute("status",
                        MessageManager.getProperty("message.incorrectPassword"));
                page = ConfigurationManager.getProperty(user != null ? "path.page.users" : "path.page.register");
                return page;
            }

            if (userAction.loginAndEmailIsExist(login, email)) {
                request.setAttribute("status",
                        MessageManager.getProperty("message.loginOrEmailIsBusy"));
                page = ConfigurationManager.getProperty(user != null ? "path.page.users" : "path.page.register");
                return page;
            }

            if (userAction.sendRegister(login, pass, email, role)) {
                request.setAttribute("status", MessageManager.getProperty("message.confirmRegistration"));
            } else {
                request.setAttribute("status",
                        MessageManager.getProperty("message.registerError"));
                page = ConfigurationManager.getProperty(user != null ? "path.page.users" : "path.page.register");
                return page;
            }
        } catch (UserException e) {
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            Logger.log(Level.ERROR, e);
        }
        return page;
    }
}
