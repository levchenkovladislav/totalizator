package by.levchenko.command.manager;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The class extracts information from the messages.properties file.
 *
 * @author Vladislav Levchenko
 */

public class MessageManager {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");

    /**
     * Changes the language in which messages are displayed.
     *
     * @param locale the desired locale
     */
    public static void changeLocale(String locale) {
        Locale bundleLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
        resourceBundle = ResourceBundle.getBundle("messages", bundleLocale);
    }

    private MessageManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}