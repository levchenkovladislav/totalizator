package by.levchenko.dao.entity;

public enum UserRole {
    GUEST, CLIENT, BOOKMAKER, ADMINISTRATOR
}