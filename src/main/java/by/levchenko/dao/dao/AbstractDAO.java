package by.levchenko.dao.dao;

import by.levchenko.dao.connectionpool.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * A class for extension classes to access database tables.
 *
 * @author Vladislav Levchenko
 */

public abstract class AbstractDAO<T> {
    protected Connection connection;
    static protected String dbName;

    public AbstractDAO(Connection connection) {
        dbName = ConnectionManager.getProperty("dbName");
        this.connection = connection;
    }

    public abstract List<T> findAll();

    public abstract T findEntityById(int id) throws SQLException;

    public abstract boolean delete(int id);

    public abstract boolean delete(T entity);

    public abstract boolean create(T entity);

    public abstract T update(T entity);
}
