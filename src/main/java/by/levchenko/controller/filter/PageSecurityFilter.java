package by.levchenko.controller.filter;

import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.dao.entity.UserRole;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A filter that protects against unauthorized access to pages.
 *
 * @author Vladislav Levchenko
 */

@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE},

        urlPatterns = {"/jsp/userjsp/userAccount.jsp", "/jsp/userjsp/sport.jsp", "/jsp/userjsp/guestjsp/login.jsp",
                "/jsp/userjsp/guestjsp/register.jsp", "/jsp/userjsp/adminjsp/users.jsp"},

        initParams = {
                @WebInitParam(name = "INDEX_PATH", value = "/index.jsp"),
                @WebInitParam(name = "USER_ACCOUNT_PATH", value = "/jsp/userjsp/userAccount.jsp"),
                @WebInitParam(name = "LOGIN_PATH", value = "/jsp/userjsp/guestjsp/login.jsp"),
                @WebInitParam(name = "REGISTER_PATH", value = "/jsp/userjsp/guestjsp/register.jsp"),
                @WebInitParam(name = "USERS_PATH", value = "/jsp/userjsp/adminjsp/users.jsp"),
                @WebInitParam(name = "SPORT_PATH", value = "/jsp/userjsp/sport.jsp"),
        })
public class PageSecurityFilter implements Filter {
    private String indexPath;

    private Map<UserRole, List<String>> roleMap = new HashMap<>();

    public void init(FilterConfig filterConfig) throws ServletException {
        indexPath = filterConfig.getInitParameter("INDEX_PATH");
        String userAccountPath = filterConfig.getInitParameter("USER_ACCOUNT_PATH");
        String loginPath = filterConfig.getInitParameter("LOGIN_PATH");
        String registerPath = filterConfig.getInitParameter("REGISTER_PATH");
        String usersPath = filterConfig.getInitParameter("USERS_PATH");
        String sportPath = filterConfig.getInitParameter("SPORT_PATH");

        roleMap.put(UserRole.GUEST, Arrays.asList(loginPath, sportPath, registerPath));
        roleMap.put(UserRole.CLIENT, Arrays.asList(userAccountPath, sportPath));
        roleMap.put(UserRole.BOOKMAKER, Arrays.asList(userAccountPath, sportPath));
        roleMap.put(UserRole.ADMINISTRATOR, Arrays.asList(userAccountPath, sportPath, usersPath));
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        request.getSession().removeAttribute("status");

        String URI = request.getRequestURI();
        UserRole userRole = UtilCommand.defineUserRole(request);


        if (!roleMap.get(userRole).contains(URI)) {
            request.getSession().setAttribute("status",
                    MessageManager.getProperty("message.noAccess"));
            response.sendRedirect(request.getContextPath() + indexPath);
            return;
        }

        chain.doFilter(servletRequest, servletResponse);
    }


    public void destroy() {
    }
}