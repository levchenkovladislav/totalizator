package by.levchenko.controller.filter;

import by.levchenko.command.manager.MessageManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A filter that protects against access to files to include.
 *
 * @author Vladislav Levchenko
 */

@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD},

        urlPatterns = {"/jsp/includejsp/*"},

        initParams = {@WebInitParam(name = "INDEX_PATH", value = "/index.jsp")})
public class IncludePageSecurityFilter implements Filter {
    private String indexPath;

    public void init(FilterConfig filterConfig) throws ServletException {
        indexPath = filterConfig.getInitParameter("INDEX_PATH");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        request.getSession().removeAttribute("status");

        request.getSession().setAttribute("status",
                MessageManager.getProperty("message.noAccess"));
        response.sendRedirect(request.getContextPath() + indexPath);
        return;
    }

    public void destroy() {

    }
}
