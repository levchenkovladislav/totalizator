package by.levchenko.controller.filter;

import by.levchenko.command.manager.MessageManager;
import by.levchenko.controller.utility.UtilController;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A filter that protects against the re-execution of a query.
 *
 * @author Vladislav Levchenko
 */

@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE},

        urlPatterns = {"/controller"},

        initParams = {@WebInitParam(name = "INDEX_PATH", value = "/index.jsp")})
public class F5Filter implements Filter {
    private String indexPath;

    public void init(FilterConfig fConfig) throws ServletException {
        indexPath = fConfig.getInitParameter("INDEX_PATH");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        request.getSession().removeAttribute("status");

        if (request.getParameter("sessId") != null && !request.getParameter("sessId").equals(String.valueOf(request.getSession().getAttribute("sessId")))) {
            request.getSession().setAttribute("status",
                    MessageManager.getProperty("message.f5"));
            response.sendRedirect(request.getContextPath() + indexPath);
            return;
        }


        request.getSession().setAttribute("sessId", UtilController.getRandomSessId());

        filterChain.doFilter(servletRequest, servletResponse);

    }

    public void destroy() {

    }
}
