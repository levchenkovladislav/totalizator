package by.levchenko.logic;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

/**
 * Methods for the logic.
 *
 * @author Vladislav Levchenko
 */

public class LogicUtil {
    /**
     * The static method for encryption password.
     *
     * @param password not encrypted password.
     * @return returns encrypted password.
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static String createEncryptedPassword(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int keyLength = 128;
        int iterationCount = 100_000;
        byte[] salt = String.valueOf(password.hashCode()).getBytes();
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, iterationCount, keyLength);
        SecretKey secretKey = keyFactory.generateSecret(keySpec);
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }
}
